//1. right click mongodb atlas create database
//2. name-hotel data base
// //3. db.rooms.insertOne({
// 	"name": "single",
// 	"accomodates": 2,
// 	"price": 1000,
// 	"description": "A simple room with all the basic necessities for one",
//         "rooms_available": 10,
//         "isAvailable":false
// });

// 4. db.rooms.insertMany(
//     [
//         {
//          "name": "double",
//          "accomodates": 2,
//          "price": 1500,
//          "description": "A simple room with all the basic necessities for two",
//              "rooms_available": 10,
//              "isAvailable":false
//         },
//         {
//          "name": "queen",
//          "accomodates": 2,
//          "price": 2000,
//          "description": "A simple room with all the basic necessities for two with meal",
//              "rooms_available": 10,
//              "isAvailable":false
//         }
        
//      ]);

// 5. db.rooms.find({"name":"double"});

// 6. db.rooms.updateOne(
//     {
//         "name":"double"
//     },
//     {
//         $set:{"rooms_available":0}
//     }
// );

// 7. db.rooms.deleteMany(
//     {
//         "rooms_available":0
//     }
// );
 

db.users.insert({
    "firstName": "Jane",
    "lastName": "Doe",
    "age":21,
    "contact":{
        "phone":"87654321",
        "email":"janedoe@gmail.com"
    },
    "courses":["CSS","Javascript","Python"],
    "department":"none"
 
});

db.users.insertMany([
    {
    "firstName": "Stephen",
    "lastName": "Hawking",
    "age":76,
    "contact":{
        "phone":"87654321",
        "email":"stephenhawking@gmail.com"
    },
    "courses":["Python","React","PHP"],
    "department":"none"
    },
    {
    "firstName": "Neil",
    "lastName": "Armstrong",
    "age":82,
    "contact":{
        "phone":"87654321",
        "email":"neilarmstrong@gmail.com"
    },
    "courses":["React","Laravel","Sass"],
    "department":"none"
    }
]);

db.users.updateMany(
    {department:"none"},
    {
        $set:{department:"HR"}
    }
)


db.users.updateOne(
    {firstName:"Test"},
    {
        $set:{
            firstName:"Bill",
            lastName:"Gates",
            age:65,
            contact:{
                phone:"12345678",
                email:"bill@gmail.com"
            },
            courses:["PHP","Laravel","HTML"],
            department:"Operations",
            status:"active"
            
        }
    }
);

db.users.replaceOne(
    {firstName:"Bill"},
    {
        firstName:"Bill",
        lastName:"Gates",
        age:65,
        contact:{
            phone:"12345678",
            email:"bill@gmail.com"
        },
        courses:["PHP","Laravel","HTML"],
        department:"Operations"
    }

);
    
